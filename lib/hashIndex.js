const crypto = require('crypto');

const getHash = (term, length) =>
  crypto.createHash('sha256').update(term).digest('hex').substr(0, length);

module.exports = class HashIndex {
  #hashes = new Set();
  #minLength = null;

  create({ name, slug, picture, locality, country }) {
    if (this.#minLength) {
      throw new Error(
        'shorten has already been called, please make sure to not add hashes after calling it'
      );
    }
    const hash = getHash([name, slug, picture, locality, country].join('|'));
    this.#hashes.add(hash);
    return hash;
  }

  #calculateMinLength = () => {
    const allValues = Array.from(this.#hashes).sort();

    this.#minLength = allValues.reduce(
      ({ prev, min }, curr) => {
        while (prev.substr(0, min) === curr.substr(0, min)) {
          min += 1;
        }

        return { prev: curr, min };
      },
      { min: 4, prev: '' }
    ).min;
  };

  shorten(key) {
    if (!this.#minLength) {
      this.#calculateMinLength();
    }

    return key.substr(0, this.#minLength);
  }
};
