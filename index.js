const fs = require('fs');

const _ = require('lodash');
const Promise = require('bluebird');

const axios = require('axios');
const querystring = require('qs');
const states = require('us-state-codes');
const yaml = require('yaml-js');

const HashIndex = require('./lib/hashIndex');

const hashIndex = new HashIndex();

const TEAM_MAP_VERSION = 9;

if (!process.env.GEONAMES_USERNAME) {
  throw new Error('env variable GEONAMES_USERNAME must be set');
}

const countriesRaw = {
  UA: ['Ukraine'],
  US: ['USA', 'Maryland', 'Uninted States', 'United States', 'U.S.A.', 'US'],
  NL: ['The Netherlands', 'Netherlands'],
  PT: ['Portugal'],
  PL: ['Poland'],
  GB: ['United Kingdom', 'UK', 'England'],
  PE: ['Peru'],
  BR: ['Brazil'],
  FR: ['France'],
  ES: ['Spain'],
  DE: ['Germany'],
  CA: ['Canada'],
  SE: ['Sweden'],
  IL: ['Israel'],
  TW: ['Taiwan'],
  CL: ['Chile'],
  AU: ['Australia'],
  NG: ['Nigeria'],
  IN: ['India'],
  MY: ['Malaysia'],
  MX: ['Mexico'],
  RS: ['Serbia'],
  ZA: ['South Africa'],
  BE: ['Belgium'],
  CZ: ['Czech Republic'],
  CN: ['China'],
  IE: ['Ireland'],
  AT: ['Austria'],
  IT: ['Italy'],
  ZW: ['Zimbabwe'],
  JP: ['Japan'],
  SI: ['Slovenia'],
  EG: ['Egypt'],
  NI: ['Nicaragua'],
  RU: ['Russia'],
  SK: ['Slovakia'],
  GR: ['Greece'],
  HU: ['Hungary'],
  LT: ['Lithuania'],
  MT: ['Malta'],
  NZ: ['New Zealand'],
  PH: ['Philippines'],
  BA: ['Bosnia and Herzegovina'],
  KE: ['Kenya'],
  PK: ['Pakistan'],
  LU: ['Luxembourg'],
  MN: ['Mongolia'],
  DK: ['Denmark'],
  BY: ['Belarus'],
  NO: ['Norway'],
  AR: ['Argentina'],
  MD: ['Moldova'],
  DO: ['Dominican Republic'],
  EC: ['Ecuador'],
  SG: ['Singapore'],
  RO: ['Romania'],
  AD: ['Andorra'],
  CH: ['Switzerland'],
  TR: ['Turkey'],
  CO: ['Colombia'],
  IS: ['Iceland'],
  KR: ['South Korea'],
  CY: ['Cyprus'],
  VN: ['Vietnam'],
  MA: ['Morocco'],
  KH: ['Cambodia'],
  BG: ['Bulgaria'],
  LK: ['Sri Lanka'],
  PY: ['Paraguay'],
  CR: ['Costa Rica'],
  PA: ['Panama']
};

const canonicalName = (code) => {
  if (!countriesRaw[code]) {
    throw new Error('Unknown country with code ' + code);
  }
  return countriesRaw[code][0];
};

const countryMap = Object.entries(countriesRaw).reduce((all, [key, entries]) => {
  return { ...all, ...Object.fromEntries(entries.map((x) => [x.toLocaleLowerCase(), key])) };
}, {});

const mapCountriesToISO = (country) => {
  const code = countryMap[country.toLocaleLowerCase()];

  if (!code) {
    throw new Error(`Unknown country: '${country}'`);
  }

  return code;
};

const geoNames = (endpoint) => async (params = {}) => {
  const response = await axios({
    method: 'get',
    url: `https://secure.geonames.org/${endpoint}`,
    params: {
      type: 'json',
      username: process.env.GEONAMES_USERNAME,
      ...params
    },
    paramsSerializer: function (params) {
      return querystring.stringify(params, { arrayFormat: 'repeat' });
    }
  });

  const status = _.get(response, 'data.status', false);
  if (status) {
    throw new Error(
      `GeoNames raised error ${status.value}: ${status.message}. Search: ${response.request.path}`
    );
  }

  return _.get(response, 'data.geonames');
};

const searchBlacklist = ['St. ', 'San '];

const startsWithBlackList = (name) => {
  if (!name) {
    return false;
  }

  if (searchBlacklist.some((b) => name.startsWith(b))) {
    return true;
  }

  return !/^[\S]+$/.test(name.substr(0, 3));
};

const searchGeoNames = geoNames('search');
const countryInfo = geoNames('countryInfo');

const STATE_CODE_REGEX = /,\s*([A-Z]+)\s*(,\s?USA?)?$/i;

function getLocality(locationName) {
  let search = (locationName && locationName.trim()) || '';

  search = search.slice(0, 1).toLocaleUpperCase() + search.slice(1);

  return search !== 'Anywhere' ? search : '';
}

const getSearchQuery = async (locationName, country) => {
  const search = (locationName !== 'TBD' && locationName) || false;
  const countryCode = mapCountriesToISO(country);

  if (search) {
    let options = {
      maxRows: 1,
      inclBbox: true,
      featureClass: ['P', 'A'],
      country: countryCode,
      orderby: 'relevance',
      q: search,
      isNameRequired: true
    };

    if (countryCode === 'US' && STATE_CODE_REGEX.test(search)) {
      let state = search.match(STATE_CODE_REGEX)[1];
      options.q = search.replace(STATE_CODE_REGEX, '');

      if (state.length !== 2) {
        state = states.getStateCodeByStateName(states.sanitizeStateName(state));
      }

      if (state.length === 2) {
        options.adminCode1 = state.toUpperCase();
      }
    }

    //Ensure that abbreviations / spaces are not used for startsWith
    if (!startsWithBlackList(search)) {
      options.name_startsWith = search.substr(0, 3);
    }

    const result = _.get(await searchGeoNames(options), '[0]', {});

    return { ...result, countryCode };
  }

  const result = _.get(await countryInfo({ country: countryCode }), '[0]', {});

  const lng =
    result.east && result.west
      ? (parseFloat(result.east) + parseFloat(result.west)) / 2
      : undefined;

  const lat =
    result.north && result.south
      ? (parseFloat(result.north) + parseFloat(result.south)) / 2
      : undefined;

  return { ...result, lat, lng, countryCode };
};

const funnyPlaceMap = {
  'Seattle, WA': 'Seattle, WA', // https://www.youtube.com/watch?v=ZKVVxYfk7Y0
  'Phila., PA': 'Philadelphia, PA',
  'Detroit Metro Area, MI': 'Detroit, MI',
  'Twin Cities, Minnesota': 'Minneapolis, Minnesota',
  'Boston Area, MA': 'Boston, MA',
  'Greater Zürich Area': 'Kanton Zürich',
  'Southeastern Pennsylvania': 'Berks County, Pennsylvania',
  'Washington, DC Metro': 'Washington, DC',
  'Los Angeles Metro, California': 'Los Angeles, CA',
  'Seoul/Suwon': 'Suwon',
  'Seoul/Seongnam': 'Seongnam',
  'Delhi-NCR': 'National Capital Territory of Delhi',
  'Saintfield, County Down, N.Ireland': 'Saintfield, Northern Ireland',
  'Chattanoga, TN': 'Chattanooga, TN',
  'Altanta, GA': 'Atlanta, GA',
  EMEA: undefined
};

const cachedLocations = {
  'Upstate, New York|USA': {
    location: [43.0, -75.5],
    countryCode: 'US',
    locality: 'Upstate New York',
    country: 'USA'
  },
  'EMEA|Ireland': {
    location: [53.415, -8.239],
    countryCode: 'IE',
    adminCode1: undefined,
    locality: undefined,
    country: 'Ireland'
  },
  'Americas East|United States': {
    location: [38, -82],
    countryCode: 'US',
    locality: undefined,
    country: 'USA'
  }
};

const roundNumber = (number) => +parseFloat(number).toFixed(3);

const getLocation = async (locationName, country) => {
  const cacheKey = `${locationName}|${country}`;

  if (cachedLocations[cacheKey]) {
    console.log(`Using cached coordinates for ${locationName} in ${country}`);
    return cachedLocations[cacheKey];
  }

  if (funnyPlaceMap[locationName]) {
    locationName = funnyPlaceMap[locationName];
  }

  const { lat, lng, countryCode, adminCode1, name } =
    (await getSearchQuery(locationName, country)) || {};

  if (!lng || !lat) {
    throw new Error(`Could not find ${locationName} in ${country}`);
  }

  cachedLocations[cacheKey] = {
    location: [roundNumber(lat), roundNumber(lng)],
    countryCode,
    adminCode1,
    locality: name,
    country: canonicalName(countryCode)
  };

  return cachedLocations[cacheKey];
};

let cached = [];

try {
  const previous = JSON.parse(fs.readFileSync('./team.json', 'utf8'));
  if (_.get(previous, 'version') === TEAM_MAP_VERSION) {
    cached = previous.team;
  }
} catch (e) {
  console.log('Could not load existing reverse coded team.json');
}

function getMemberPicture(picture) {
  if (picture.startsWith('https://')) {
    return picture;
  }
  const pictureName = picture.replace(/\.(png|jpe?g)$/gi, '');
  return `https://about.gitlab.com/images/team/${pictureName}-crop.jpg`;
}

function compareMembers(a, b) {
  const keyLength = Math.min(a.key.length, b.key.length);

  return a.key.substr(0, keyLength) === b.key.substr(0, keyLength);
}

const mapMember = async (member) => {
  const hash = hashIndex.shorten(member.key);

  if (cached[hash]) {
    console.log(`Cached result for ${member.name}`);
    return cached[hash];
  }

  console.log(
    `Searching location for ${member.name}: ${member.locality || 'No locality'} in ${
      member.country
    }`
  );

  let result;
  try {
    result = await getLocation(member.locality, member.country);
  } catch (e) {
    throw new Error(`Could not load location for ${member.name}:\n\t${e}`);
  }

  const { location, countryCode, adminCode1, locality, country } = result;
  console.log(
    `${member.name}; ${member.locality || 'No locality'}, ${
      member.country
    }: ${location} — ${country}`
  );

  return {
    key: hash,
    slug: member.slug,
    name: member.name,
    location,
    countryCode,
    stateCode: countryCode === 'US' ? adminCode1 : null,
    locality,
    country,
    picture: member.picture
  };
};

function bail(e) {
  if (e && e.stack) {
    console.warn(e.stack);
  }
  console.warn(`Error creating the team page map:\n\t${e}`);
  process.exit(1);
}

// Get document, or throw exception on error
async function main() {
  const doc = yaml.load(fs.readFileSync('./team.yml', 'utf8'));

  const members = doc
    .filter(
      (member) =>
        member.type !== 'vacancy' &&
        member.country &&
        member.country !== 'Remote' &&
        member.slug !== 'open-roles' &&
        member.picture !== '../gitlab-logo-extra-whitespace.png'
    )
    .map((member) => {
      const locality = getLocality(member.locality);
      const picture = getMemberPicture(member.picture);
      const res = { ...member, locality, picture };
      res.key = hashIndex.create(member);
      return res;
    })
    .sort(function (a, b) {
      if (a.start_date > b.start_date) {
        return 1;
      }
      if (a.start_date < b.start_date) {
        return -1;
      }
      return 0;
    });

  const tooMuch = _.differenceWith(cached, members, compareMembers);

  if (tooMuch.length > 0) {
    console.log(`Going to remove ${tooMuch.map((x) => x.name).join(', ')}`);
    cached = _.difference(cached, tooMuch);
  }

  cached = _.keyBy(cached, 'key');

  const team = await Promise.map(members, mapMember, { concurrency: 1 });

  console.log('\nFound a location for all team members');
  const result = { version: TEAM_MAP_VERSION, team };
  return fs.writeFileSync('./team.json', JSON.stringify(result));
}

main()
  .then(() => {
    console.log('Mapped all members on the team page and wrote results to team.json');
    process.exit(0);
  })
  .catch(bail);
